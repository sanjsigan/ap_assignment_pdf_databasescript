CREATE DATABASE  IF NOT EXISTS `bookstore` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bookstore`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bookstore
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `BookNumber` int(11) NOT NULL,
  `BookName` varchar(45) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  PRIMARY KEY (`BookNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (251,'Java',1500),(500,'Ramayanam',650),(521,'Networking',2500);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookstore`
--

DROP TABLE IF EXISTS `bookstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookstore` (
  `CustomerId` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(45) DEFAULT NULL,
  `Gender` enum('Male','Female') DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  `MobileNumber` varchar(45) DEFAULT NULL,
  `CreatePassword` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CustomerId`)
) ENGINE=InnoDB AUTO_INCREMENT=826 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookstore`
--

LOCK TABLES `bookstore` WRITE;
/*!40000 ALTER TABLE `bookstore` DISABLE KEYS */;
INSERT INTO `bookstore` VALUES (29,'sanjsigan','Male','Jaffna','0767048566','san@3016'),(125,'Shanthan','Male','Jaffna','02145895','446564'),(244,'sanjsiga','Female','daadadada','46464644','sasaasa'),(371,'dadada','Male','rwrwrw','646664','sadada'),(517,'Sakkk','Female','ada','fwfwfw','dadada'),(825,'Saavithri','Male','Mannar','65446646','466646');
/*!40000 ALTER TABLE `bookstore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buybook`
--

DROP TABLE IF EXISTS `buybook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buybook` (
  `BookNumber` int(11) NOT NULL,
  `BookName` varchar(45) DEFAULT NULL,
  `UserName` varchar(45) DEFAULT NULL,
  `HowManyBooks` int(11) DEFAULT NULL,
  `Price` varchar(45) DEFAULT NULL,
  `TotalPrice` int(11) DEFAULT NULL,
  `Creditcard` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buybook`
--

LOCK TABLES `buybook` WRITE;
/*!40000 ALTER TABLE `buybook` DISABLE KEYS */;
INSERT INTO `buybook` VALUES (521,'Ramayanam','Sanjsigan',NULL,'650',NULL,'665659656'),(321,'Ramayanam','Sanjsigan',NULL,'500',NULL,'5456'),(500,'Java','Sanjsigan',5,'500',2500,'646465'),(500,'Ramayanam','sanjisgan',10,'650',6500,'1464464'),(500,'Ramayanam','sadasad64',10,'650',6500,'64646664646'),(500,'Ramayanam','sanjsigan',10,'650',6500,'5464631'),(500,'Ramayanam','sanjsigan',10,'650',6500,'5667964641'),(251,'Java','sanjsgan',5,'1500',7500,'46746464'),(500,'Ramayanam','sanjisgan',5,'650',3250,'4546'),(500,'Ramayanam','sanisgan',5,'650',3250,'47466'),(251,'Java','sanjsigan',250,'1500',375000,'231311');
/*!40000 ALTER TABLE `buybook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buydvd`
--

DROP TABLE IF EXISTS `buydvd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buydvd` (
  `DvdNumber` int(11) NOT NULL,
  `DvdName` varchar(45) DEFAULT NULL,
  `UserName` varchar(45) DEFAULT NULL,
  `HowmanyDVDs` int(11) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `TotalPrice` int(11) DEFAULT NULL,
  `CreditCardno` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buydvd`
--

LOCK TABLES `buydvd` WRITE;
/*!40000 ALTER TABLE `buydvd` DISABLE KEYS */;
INSERT INTO `buydvd` VALUES (321,'Mahabaratha','Sanjsigan',NULL,500,NULL,'21548463496'),(321,'Mahabaratha','Kannan',NULL,500,NULL,'4959859'),(321,'mahabaratha','thanush',NULL,550,NULL,'2311123344444444'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'6461465'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'1148446'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'6446464'),(321,'mahabaratha','sanjsi',NULL,550,NULL,'54544'),(321,'Mahabaratha','Santhau',NULL,550,NULL,'5+58+85+6461212'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'5545'),(321,'sqqdd','sadsq',NULL,550,NULL,'5646'),(321,'xsca','sasa',NULL,550,NULL,'5656646'),(321,'MAHABARATHA','ssasa',NULL,550,NULL,'5655656'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'1515'),(321,'MAHUHUH','DWDW',NULL,550,NULL,'56564'),(321,'xsdsds','dsds',NULL,566,NULL,'554646'),(321,'Mahabaratha','sanjsigan',NULL,550,NULL,'5648795622'),(321,'Mahabaratha','Thanushan',NULL,550,NULL,'6494646'),(321,'Mahabaratha','niro ',NULL,550,NULL,'5+6+56416'),(321,'Mahabaratham','Sanjsigan',NULL,550,NULL,'+659794646'),(321,'Mahabaratham','Sanjsigan',NULL,550,NULL,'+659794646'),(321,'mahabaratha','sanjsi',NULL,550,NULL,'665652'),(321,'mahabaratha','sanjsi',NULL,550,NULL,'665652'),(321,'mahabaratha','sanjsi',NULL,550,NULL,'665652'),(321,'mahabaratha','sanjsi',NULL,550,NULL,'665652'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'16446464'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'369+646644'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'654626563'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'656464'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'2+22262'),(321,'Mahabaratha','Sanjsigan',NULL,550,NULL,'56564'),(321,'Mahabaratha','Sanjsi',NULL,550,NULL,'265961'),(321,'Mahabaratha','Sanjsigan',5,550,2750,'6464646'),(321,'Mahabaratha','Sanjsigan',10,550,5500,'64564646'),(321,'Mahabaratha','Sanjsigan',5,550,2750,'5694649+6'),(500,'Mahabaratha','sanjsigan',5,550,2750,'41646416'),(321,'Mahabaratha','santhanu',10,550,5500,'546464'),(321,'Mahabaratha','Sanjsigan',5,550,2750,'4646464'),(321,'Mahabaratha','Sanjsigan',5,550,2750,'5634646'),(321,'Mahabaratha','sanjsigan',5,550,2750,'6+4696446'),(321,'Mahabaratha','sanjsigan',5,550,2750,'1464646'),(321,'Mahabaratha','Sanjsigan',5,550,2750,'4341313'),(321,'Mahabaratha','sanjsigan',10,550,5500,'222'),(321,'Mahabaratha','sanjsigan',10,550,5500,'1646679523'),(252,'Mysql','sanjsigan',10,1200,12000,'254641+3'),(500,'Windows10','sanisi',5,1200,6000,'56466'),(252,'Mysql','sanjsigan',5,1200,6000,'547851223'),(500,'Windows10','sanjsigan',10,1200,12000,'6413464'),(252,'Mysql','sanjsigan',10,1200,12000,'659466466');
/*!40000 ALTER TABLE `buydvd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dvd`
--

DROP TABLE IF EXISTS `dvd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dvd` (
  `DVDNo` int(11) NOT NULL,
  `DVDName` varchar(45) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  PRIMARY KEY (`DVDNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dvd`
--

LOCK TABLES `dvd` WRITE;
/*!40000 ALTER TABLE `dvd` DISABLE KEYS */;
INSERT INTO `dvd` VALUES (252,'Mysql',1200),(321,'Mahabaratha',550);
/*!40000 ALTER TABLE `dvd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bookstore'
--

--
-- Dumping routines for database 'bookstore'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-18 11:41:26
